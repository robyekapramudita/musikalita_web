import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkExactActiveClass: 'disabled active',
  routes: [
    {
      path: '/',
      name: 'user',
      component: () => import('@/components/Dashboard/Index')
    }
  ]
})
